import numpy as np

def txtResultReader( results ):
    dataList = []
    tmpID = 0
    ID = 0
    
    with open (results,'r') as f: 
        for line in f:
           if not line.startswith('#'):
                words   = line.split( )
                
                tmpID   = words[0]
                
                dataSeg = [int(words[3])]
                dataSeg.append(int(words[4]))
                dataSeg.append(words[6])
                dataSeg.append(words[8].split('_')[0])
                dataList.append(dataSeg)
    
    ID = tmpID.split('|')[1]

    if ID != 0:
        return (ID,dataList)
    else:
        print ID

ID, ourEst = txtResultReader('results.txt')  # test txt 
print type(ID)
print ID
print ''
print ourEst

'''
[ailaka@nordur code]$ python txtResultReader.py 
<type 'str'>
255534169

[[433939, 436706, '+', '23s'],
[2233027, 2235794, '-', '23s'],
[436856, 436962, '+', '5s'],
[2232771, 2232877, '-', '5s'],
[431637, 433141, '+', '16s'],
[2236592, 2238096, '-', '16s']]
'''
