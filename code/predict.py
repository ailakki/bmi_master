from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Alphabet import *

import shlex, subprocess



from BioSQL import BioSeqDatabase
import sys


username = raw_input("Please enter user name: ")
password = raw_input("and password: ")

server = BioSeqDatabase.open_database(driver = "psycopg2", user = username, passwd = password, host = "dbpg-ifi-utv.uio.no", db = "rnammer")


def gffLoader(seqdata,gffFile,biodb_name_org):
    
    # gloabal default
    biodb_name  = "biodb_name"  # dummy name
    db          = "no_biodb"

    # Step 1 making secRecord

    name        = seqdata
    acc_id      = name.split(".")[0]
    version     = name.split(".")[1]
    description = "Genome description and info" 

    #gi_id = ??

    S   = Seq("CATTGTTGAGATCACATAATAATTGATCGA", DNAAlphabet())

    rec = SeqRecord(
            S, 
            id= acc_id, 
            name= name,
            description= "empty", 
            dbxrefs=[]
            )

    gff_File    = open(gffFile, 'r')
    #gffLine     = []
    
    for line in gff_File:
        if not line.startswith('#'):
            #print line
            col = line.split( )
            #print col[0]
            #print col[1]
            # create db if none exist
            if biodb_name != '_'.join([col[1], biodb_name_org]) or biodb_name == "testsource":
                biodb_name = '_'.join([col[1], biodb_name_org])
                while True : 
                    try :
                        db = server[biodb_name] ## append biodb_name
                        print "New db name: {}".format(biodb_name)
                        break
                    except KeyError :
                        #print ("Cannot find biodatabase with name %r making it" % biodb_name)
                        server.new_database(biodb_name)
                        server.commit()
                        #server.remove_database(biodb_name)
                        
            # controll one per gff input to global 
            if acc_id != col[0].split('.')[0]:   #test if accesseion nr set assume NC_012659.1 format	
                acc_id  = col[0].split('.')[0] 
                name    = col[0]
            
                # make seqRecord if none made so feature of rRNA can be added
                # #S   = server["refseq"].lookup(gi= gi_id).format("fasta") # add apllhabet

                S   = Seq("CATTGTTGAGATCACATAATAATTGATCGA", DNAAlphabet())
                       
                rec = SeqRecord(
                        S, 
                        id= acc_id, 
                        name= name,
                        description= description, 
                        dbxrefs=[]
                        )

                # Error duplicate key value violates unique constraint 
                # "bioentry_accession_biodatabase_id_version_key"
                # DETAIL:  Key (accession, biodatabase_id, version)=(nc_001, 57, 0) already exists.
                # meaning gi nr and accession nr must not already exist in the database

    
                # Step 2 adding gi_id
                
                #rec.annotations["gi"] = gi_id       # important for GI nummber
                #db.load([rec])
                #server.commit()



            # Step 3 appending features needs 
            # TODO make a loop for 1< feature per genome 
            #rec = server[dbname].lookup(gi= gi_id)


            f_type 	    = col[2]                    # feature type NB spellig important !? !
            start 	    = int(col[3])               # start of feature (rRNA) in the sequence 
            end 	    = int(col[4])               # end of feature (rRNA) in the sequence
            #score          = col[5]                    # score 
            if col[6]=='+' :
                strand  = 1
            else :
                strand = -1                             # + or -, defined by 1 or -1 direction
            #frame          = col[7]                    # frame
            attribute       = col[8]                    #"23S ribosomal RNA"    


            product = {"product": attribute}            # soooo hard to figur out

            rec.features.append(
                    SeqFeature(
                        FeatureLocation(
                            start, 
                            end, 
                            strand= strand
                            ), 
                        type = f_type, 
                        qualifiers = product
                        )
                    )

            #feature_location = FeatureLocation(start, stop, strand= strand)
            #rRNA_features = SeqFeature(feature_location, type = f_type, qualifiers = product) 
            #rec.features.append( rRNA_features )

    if db != "no_biodb" :
        db.load([rec])
    else :
        print acc_id
        print "not rRNA"
    server.adaptor.commit()

def RNAmmer(seqdata, biodb):
        # takes ID of genom goes in to bioSQL and get it runs 

        db = server[biodb]             # should be possible to input
        
        name    = seqdata
        acc_id  = name.split(".")[0]
        version = name.split(".")[1]

        print acc_id 
        
        seq_record = db.lookup(accession= acc_id).format("fasta") # extraxt string of only alphabet acgttgca
        

        fasta_file_ish = seq_record # "%s \n%s" %(first_line,seq_record)

        f = open("tmpRnammmerFasta.txt","w")
        f.write(fasta_file_ish)
        f.close()
        
        # TODO check if genbank ID or fasta file
        args_str = "perl rnammer/rnammer -S bac -m lsu,ssu,tsu -gff - "
                
        args = shlex.split(args_str + "tmpRnammmerFasta.txt") 

        with open('r2.txt', 'wb', 0) as file:
            subprocess.call(args, stdout=file)







biodb_names     = ["Actinobacteria","Bacteroidetes","Chlamydiae","Cyanobacteria","Deinococcus-Thermus","Firmicutes","Spirochaetes","Tenericutes"]

singles         = ["Aquificae","Chlorobi","Chloroflexi","Dictyoglomi","Fusobacteria","Nitrospirae","Planctomycetes","Synergistetes","Thermotogae"]

Proteobacteria  = ["Alphaproteobacteria","Betaproteobacteria","Gammaproteobacteria","Deltaproteobacteria","Epsilonproteobacteria"]




def predict(biodb):
    db = server[biodb]
    
    print "This database contains %i records" % len(db)
    count = 1
    for key, record in db.iteritems():
        print "Key %r maps to a sequence record with id %s" % (key, record.id)
        RNAmmer(record.id,biodb)
        print count
        count = count +1
        gffLoader(record.id,"r2.txt",biodb)
        print "Done"

for db in singles :
    predict(db)

