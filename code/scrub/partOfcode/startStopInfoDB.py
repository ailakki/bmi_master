from BioSQL import BioSeqDatabase
import sys


username = raw_input("Please enter user name: ")
password = raw_input("and password: ")

server = BioSeqDatabase.open_database(driver = "psycopg2", user = username, passwd = password, host = "dbpg-ifi-utv.uio.no", db = "rnammer")


db = server["rnammerTEST"]                 #  db name will probably be the same
seq_record = db.lookup(gi="556503834")  # identefier to get all we will write a loop


tmp = seq_record.features               # tmp to see what is happening

rRNAs = [x for x in tmp if x.type == "rRNA"]    #

orgEst = []
import re
for r in rRNAs:
    tmp = str(r.location)
    tmp2 = re.split(r'[()\[\]:]', tmp)
    tmp2.remove('')
    tmp2.remove('')
    tmp2.append(str(r.qualifiers["product"]).split()[0][2:])
    tmp2.remove('')
    orgEst.append(tmp2)
print orgEst

