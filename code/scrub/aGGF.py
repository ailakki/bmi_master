
import pprint
from BCBio.GFF import GFFExaminer
 
in_file = "results.txt"
examiner = GFFExaminer()
in_handle = open(in_file)
pprint.pprint(examiner.available_limits(in_handle))
in_handle.close()

print'-------------------------------------------------------------'
from BCBio import GFF
 
in_file = "results.txt"
 
in_handle = open(in_file)
for rec in GFF.parse(in_handle):
    print rec
in_handle.close()
print '---------------------------------------------------------------'


limit_info = dict(
        gff_id = ["chr1"],
        gff_source = ["Coding_transcript"])
 
in_handle = open(in_file)
for rec in GFF.parse(in_handle, limit_info=limit_info):
    print rec.features[0]
in_handle.close()

print '---------------------------------------------------------------'


in_handle = open(in_file)
for rec in GFF.parse(in_handle, target_lines=1000):
    print rec
in_handle.close()

