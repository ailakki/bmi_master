
def makeStaticData( baseList, newList):
    baseList = sorter(baseList)
    newList = sorter(newList)
    
    for x in range(len(baseList)):

        # If list of list from getPostgresFeatures() 
        #
        start   =   abs(float(baseList[x][0])) -abs(float(newList[x][0]))
        end     =   abs(float(baseList[x][1])) -abs(float(newList[x][1]))
        ty      =   baseList[x][3]
        d       =   baseList[x][2] 
        print   'Start diff is {0} from orginal, stop diff is{1} and the direction is {2}. The type is {3}"'.format(start, end, d,ty)

def sorter(L):
    from operator import itemgetter

    return sorted(L, key=itemgetter(0))


makeStaticData()

