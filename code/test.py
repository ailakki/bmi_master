from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Alphabet import IUPAC


from BioSQL import BioSeqDatabase
import sys
#import seaborn
import numpy as np
import scipy.stats as stats
#import pylab as pl
import matplotlib.pyplot as pl
from BioSQL import BioSeqDatabase
import sys
from operator import itemgetter

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import gaussian_kde

username = raw_input("Please enter user name: ")
password = raw_input("and password: ")

server = BioSeqDatabase.open_database(driver = "psycopg2", user = username, passwd = password, host = "dbpg-ifi-utv.uio.no", db = "rnammer")

ex = ["NC_000913","NC_002737","NC_002506","NC_002505","NC_002946","NC_002516"]#,"NC_011750","NC_004603"]#,"NC_003197","NC_004668","NC_000913","NC_014644","NC_003197"]  

def down(biodb):
    db = server[biodb]
    
    print "This database contains %i records" % len(db)
    count = 1
    for i in ex:
    #for key, record in db.iteritems():
        record =  db.lookup(accession= i)
        #print "Key {} maps to a sequence record with id {}".format(key, record.id)
        print "Looking at {}".format(record.id)
        print type(record)
        print record
        print'--------------------------'
        print record.annotations["source"]
        print record.annotations["taxonomy"]
        #print record.annotations["origin"]

        print count
        count = count +1
        #gffLoader(record.id,"r2.txt")
        #print "Done"
down("testseq")



IDs =   ["NC_011750","NC_004603","NC_004668","NC_000913",
         "NC_000117","NC_007677","NC_004307","NC_014923",
         "NC_000912","NC_013172","NC_004722","NC_000963",
         "NC_004116","NC_015663","NC_009495","NC_000918",
         "NC_002662","NC_002737","NC_003454","NC_013853",
         "NC_007493","NC_002163","NC_005945","NC_005027",
         "NC_006351","NC_001318","NC_003198","NC_007005",
         "NC_017634","NC_004461","NC_004463","NC_004605",
         "NC_017384","NC_000915","NC_010397","NC_016603", 
         "NC_000907","NC_002932","NC_007606","NC_007494",
         "NC_006814","NC_003030","NC_001263","NC_000922",
         "NC_005125","NC_003902","NC_013522","NC_010287",
         "NC_018828","NC_009636","NC_002695","NC_003112",
         "NC_003098","NC_010175","NC_014318","NC_012587",
         "NC_006055","NC_003210","NC_014638","NC_002967",
         "NC_003063","NC_006349","NC_018658","NC_004578",
         "NC_017386","NC_008596","NC_004567","NC_008570",
         "NC_002937","NC_007643","NC_006348","NC_011296",
         "NC_009089","NC_011751","NC_007644","NC_006840",
         "NC_014644","NC_003143","NC_002947","NC_004350", 
         "NC_006570","NC_009009","NC_005364","NC_002528",
         "NC_003997","NC_004663","NC_000962","NC_009613",
         "NC_004347","NC_009698","NC_008526","NC_000853",
         "NC_003062","NC_007929","NC_004337","NC_002505",
         "NC_002939","NC_002946","NC_008800","NC_011916",
         "NC_002677","NC_006347","NC_004342","NC_005957",
         "NC_007795","NC_003450","NC_003047","NC_006841",
         "NC_003197","NC_006350","NC_002696","NC_019382",
         "NC_012926","NC_006461","NC_002516","NC_003888",
         "NC_004113","NC_017960","NC_014121","NC_005042",
         "NC_002942","NC_016845","NC_011661","NC_000964",
         "NC_002971","NC_002929"]

